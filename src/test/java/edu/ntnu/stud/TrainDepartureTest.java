package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for the TrainDeparture class.
 */ 
public class TrainDepartureTest {
  private TrainDeparture trainDeparture;

  /**
   * Creates a new TrainDeparture-object before each test.
   */
  @BeforeEach
  public void setUp() {
    trainDeparture = new TrainDeparture(
        LocalTime.of(10, 0),
        "Line A",
        123,
        "Destination X",
        1,
        Duration.ofMinutes(5)
    );
  }


  @Test
  public void testGetDepartureTime() {
    assertEquals(LocalTime.of(10, 0), trainDeparture.getDepartureTime());
  }

  @Test
  public void testGetLine() {
    assertEquals("Line A", trainDeparture.getLine());
  }

  @Test
  public void testGetTrainNumber() {
    assertEquals(123, trainDeparture.getTrainNumber());
  }

  @Test
  public void testGetDestination() {
    assertEquals("Destination X", trainDeparture.getDestination());
  }

  @Test
  public void testGetTrack() {
    assertEquals(1, trainDeparture.getTrack());
  }

  @Test
  public void testGetDelay() {
    assertEquals(Duration.ofMinutes(5), trainDeparture.getDelay());
  }

  @Test
  public void testSetDelay() {
    trainDeparture.setDelay(Duration.ofMinutes(10));
    assertEquals(Duration.ofMinutes(10), trainDeparture.getDelay());
  }

  @Test
  public void testSetNegativeDelay() {
    // Checks if an IllegalArgumentException is thrown when we try to set a negative delay
    assertThrows(IllegalArgumentException.class, () -> {
      trainDeparture.setDelay(Duration.ofMinutes(-5));
    });
  }

  @Test
  public void testConstructorWithNegativeTrainNumber() {
    // Checks if an IllegalArgumentException is thrown when given a negative trainNumber
    assertThrows(IllegalArgumentException.class, () -> {
      new TrainDeparture(
        LocalTime.of(12, 0),
        "Line B",
        -1,
        "Destination Y",
        2
        );
    });
  }

  @Test
  public void testConstructorWithNegativeTrack() {
    // Checks if an IllegalArgumentException is thrown when given a negative tracknumber
    assertThrows(IllegalArgumentException.class, () -> {
      new TrainDeparture(
        LocalTime.of(14, 0),
        "Line C",
        456,
        "Destination Z",
        -3
      );
    });
  }

  @Test
  public void testDefaultDelay() {
    TrainDeparture train = new TrainDeparture(
        LocalTime.of(8, 0),
        "Line D",
        789,
        "Destination W",
        3);
    assertEquals(Duration.ofMinutes(0), train.getDelay());
  }

  @Test
  public void testSetNegativeDelayOnExistingObject() {
    // Create a TrainDeparture object with a non-negative delay
    TrainDeparture train = new TrainDeparture(
        LocalTime.of(9, 0),
        "Line E",
        101,
        "Destination Y",
        2,
        Duration.ofMinutes(10));

    // Try to set a negative delay and ensure it throws an exception
    assertThrows(IllegalArgumentException.class, () -> {
      train.setDelay(Duration.ofMinutes(-5));
    });
    
    // Verify that the delay is still the same as before
    assertEquals(Duration.ofMinutes(10), train.getDelay());
  }

  @Test
  public void testSetValidTrackNumber() {
    trainDeparture.setTrack(3);
    assertEquals(3, trainDeparture.getTrack());
  }

  @Test
  public void testSetTrackNumberNegative() {
    assertThrows(IllegalArgumentException.class, () -> {
      trainDeparture.setTrack(-2);
    });
  }

  @Test
  public void testSetTrackNumberToNegativeOne() {
    trainDeparture.setTrack(-1);
    assertEquals(-1, trainDeparture.getTrack());
  }

  @Test
  public void testToString() {
    String expectedOutput = "---------- Train departure ----------\n";
    expectedOutput += "Departure time: 10:00\n";
    expectedOutput += "Train number: 123\n";
    expectedOutput += "Train line: Line A\n";
    expectedOutput += "Track: 1\n";
    expectedOutput += "Destination: Destination X\n";
    expectedOutput += "Delay: 00:05\n";

    assertEquals(expectedOutput, trainDeparture.toString());
  }

  @Test
  public void testToStringWithoutDelay() {
    String expectedOutput = "---------- Train departure ----------\n";
    expectedOutput += "Departure time: 08:00\n";
    expectedOutput += "Train number: 789\n";
    expectedOutput += "Train line: Line D\n";
    expectedOutput += "Track: 3\n";
    expectedOutput += "Destination: Destination W\n";
    expectedOutput += "Delay: 00:00\n";
    
    TrainDeparture train = new TrainDeparture(
        LocalTime.of(8, 0),
        "Line D",
        789,
        "Destination W",
        3);

    assertEquals(expectedOutput, train.toString());
  }

  @Test
  public void testStringTableFormatWithDelay() {
    String expectedOutput = "10:00\t\tLine A\t123\t\tDestination X\t00:05\t1\t";

    assertEquals(expectedOutput, trainDeparture.stringTableFormat());
  }

  @Test
  public void testStringTableFormatWithoutDelay() {
    // Create a TrainDeparture without delay
    TrainDeparture train = new TrainDeparture(
            LocalTime.of(8, 0),
            "Line D",
            789,
            "Destination W",
            3);

    String expectedOutput = "08:00\t\tLine D\t789\t\tDestination W\t\t3\t";

    assertEquals(expectedOutput, train.stringTableFormat());
  }

  @Test
  public void testStringTableFormatWithTrackNegativeOne() {
    TrainDeparture train = new TrainDeparture(
            LocalTime.of(12, 0),
            "Line B",
            456,
            "Destination Y",
            -1);

    String expectedOutput = "12:00\t\tLine B\t456\t\tDestination Y\t\t";

    assertEquals(expectedOutput, train.stringTableFormat());
  }
}
