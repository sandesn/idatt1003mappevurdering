package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for the DepartureRegister class.
 */ 
public class DepartureRegisterTest {
  private DepartureRegister register;

  /**
   * Creates a new DepartureRegister-object.
   */
  @BeforeEach
  public void setUp() {
    register = new DepartureRegister();
  }
  
  @Test
  public void testregisterNewDepartureToEmptyRegister() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    
    register.registerNewDeparture(departure1);
    
    assertTrue(register.getDepartures().contains(departure1));
  }
  
  @Test
  public void testregisterNewDepartureUniqeTrainNumbers() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));
    
    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);
    
    assertTrue(register.getDepartures().contains(departure1));
    assertTrue(register.getDepartures().contains(departure2));
  }
  
  @Test
  public void testregisterNewDepartureDuplicateTrainNumbers() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 101,
        "Station Y", 2, Duration.ofMinutes(0));
    
    register.registerNewDeparture(departure1);
    assertTrue(register.getDepartures().contains(departure1));
    
    assertThrows(IllegalArgumentException.class, () -> {
      register.registerNewDeparture(departure2);
    });
  }
  
  @Test
  public void testDoesNumberExistPositive() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
        
    register.registerNewDeparture(departure1);

    assertEquals(register.doesNumberExist(101), true);
  }
      
  @Test
  public void testDoesNumberExistNegative() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    
    register.registerNewDeparture(departure1);

    assertEquals(register.doesNumberExist(202), false);
  }

  @Test
  public void testGetDepartureOnNumberPositive() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    
    register.registerNewDeparture(departure1);

    assertEquals(register.getDepartureOnNumber(101), departure1);
  }

  @Test
  public void testGetDepartureOnNumberNegative() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    
    register.registerNewDeparture(departure1);

    assertEquals(register.getDepartureOnNumber(123), null);
  }

  @Test
  public void testGetDeparturesOnDestinationWithEmptyRegister() {
    List<TrainDeparture> departures = register.getDeparturesOnDestination("Station X");
    assertTrue(departures.isEmpty());
  }
  
  @Test
  public void testGetDeparturesOnDestinationWithNonEmptyRegister() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(12, 15), "Line C", 303,
        "Station X", 3);

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);
    register.registerNewDeparture(departure3);

    List<TrainDeparture> departuresToStationX = register.getDeparturesOnDestination("Station X");
    
    assertEquals(2, departuresToStationX.size(), "Should return 2 departures to Station X");
    assertTrue(departuresToStationX.contains(departure1), "Should contain departure1");
    assertTrue(departuresToStationX.contains(departure3), "Should contain departure3");
    
    List<TrainDeparture> departuresToStationY = register.getDeparturesOnDestination("Station Y");
    assertEquals(1, departuresToStationY.size(), "Should return 1 departure to Station Y");
    assertTrue(departuresToStationY.contains(departure2), "Should contain departure2");
  }

  @Test
  public void testGetSortedListWithEmptyRegister() {
    List<TrainDeparture> sortedList = register.getSortedList();
    assertTrue(sortedList.isEmpty());
  }

  @Test
  public void testGetSortedListWithNonEmptyRegister() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(9, 45), "Line C", 303,
        "Station X", 3);

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);
    register.registerNewDeparture(departure3);

    List<TrainDeparture> sortedList = register.getSortedList();

    assertEquals(sortedList.size(), 3);
    assertEquals(sortedList.get(0), departure3);
    assertEquals(sortedList.get(1), departure1);
    assertEquals(sortedList.get(2), departure2);
  }

  @Test
  public void testRemoveDepartedWithNonEmptyRegister() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(12, 15), "Line C", 303,
        "Station X", 3);
    TrainDeparture departure4 = new TrainDeparture(LocalTime.of(13, 45), "Line D", 404,
        "Station W", 4, Duration.ofMinutes(10));

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);
    register.registerNewDeparture(departure3);
    register.registerNewDeparture(departure4);

    LocalTime currentTime = LocalTime.of(12, 0);
    register.removeDeparted(currentTime);

    List<TrainDeparture> remainingDepartures = register.getDepartures();

    assertEquals(remainingDepartures.size(), 2);
    assertTrue(remainingDepartures.contains(departure3));
    assertTrue(remainingDepartures.contains(departure4));
  }

  @Test
  public void testRemoveDepartedEdgeCase() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(23, 0), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(120));

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);

    LocalTime currentTime = LocalTime.of(12, 0);
    register.removeDeparted(currentTime);

    List<TrainDeparture> remainingDepartures = register.getDepartures();
    assertEquals(remainingDepartures.size(), 1);
    assertTrue(remainingDepartures.contains(departure2));
  }

  @Test
  public void testGetTrainNumbersEmptyRegister() {
    List<Integer> trainNumbers = register.getTrainNumbers();
    assertTrue(trainNumbers.isEmpty());
  }

  @Test
  public void testGetTrainNumbersNonEmptyRegister() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(12, 15), "Line C", 303,
        "Station X", 3);

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);
    register.registerNewDeparture(departure3);

    List<Integer> trainNumbers = register.getTrainNumbers();

    assertEquals(3, trainNumbers.size());
    assertTrue(trainNumbers.contains(101));
    assertTrue(trainNumbers.contains(202));
    assertTrue(trainNumbers.contains(303));
  }

  @Test
  public void testGetTrainNumbersAfterRemoval() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(12, 15), "Line C", 303,
        "Station X", 3);

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);
    register.registerNewDeparture(departure3);

    register.removeOnTrainNumber(202); // Removing departure2 with train number 202

    List<Integer> trainNumbersAfterRemoval = register.getTrainNumbers();

    assertEquals(2, trainNumbersAfterRemoval.size());
    assertTrue(trainNumbersAfterRemoval.contains(101));
    assertFalse(trainNumbersAfterRemoval.contains(202));
    assertTrue(trainNumbersAfterRemoval.contains(303));
  }

  @Test
  public void testRemoveOnTrainNumberExistingDeparture() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));

    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure2);

    register.removeOnTrainNumber(101);

    List<TrainDeparture> remainingDepartures = register.getDepartures();
    assertEquals(1, remainingDepartures.size());
    assertFalse(remainingDepartures.contains(departure1));
    assertTrue(remainingDepartures.contains(departure2));
  }

  @Test
  public void testRemoveOnTrainNumberNonExistingDeparture() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));

    register.registerNewDeparture(departure1);

    assertThrows(IllegalArgumentException.class, () -> {
      register.removeOnTrainNumber(202); // Attempting to remove a non-existing departure
    });

    List<TrainDeparture> remainingDepartures = register.getDepartures();
    assertEquals(1, remainingDepartures.size());
    assertTrue(remainingDepartures.contains(departure1));
  }

  @Test
  public void testSetTrackOnTrainNumberExistingDeparture() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));

    register.registerNewDeparture(departure1);

    register.setTrackOnTrainNumber(3, 101); // Set track number 3 for departure1

    assertEquals(3, register.getDepartureOnNumber(101).getTrack());
  }

  @Test
  public void testSetTrackOnTrainNumberNonExistingDeparture() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));

    register.registerNewDeparture(departure1);

    assertThrows(IllegalArgumentException.class, () -> {
      register.setTrackOnTrainNumber(4, 202);
    });

    assertEquals(1, register.getDepartureOnNumber(101).getTrack());
  }

  @Test
  public void testSetDelayOnTrainNumberExistingDeparture() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", 2, Duration.ofMinutes(0));

    register.registerNewDeparture(departure1);

    register.setDelayOnTrainNumber(Duration.ofMinutes(10), 202);

    assertEquals(Duration.ofMinutes(10), register.getDepartureOnNumber(202).getDelay());
  }

  @Test
  public void testSetDelayOnTrainNumberNonExistingDeparture() {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", 1, Duration.ofMinutes(5));

    register.registerNewDeparture(departure1);

    assertThrows(IllegalArgumentException.class, () -> {
      register.setDelayOnTrainNumber(Duration.ofMinutes(8), 303);
    });

    assertEquals(Duration.ofMinutes(5), register.getDepartureOnNumber(101).getDelay());
  }
}
