package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class serves as a register of {@code TrainDeparture} objects.
 * It stores and provides methods to manipulate train departure information.
 *
 * <p>{@code DepartureRegister} acts as a container for storing {@code TrainDeparture} objects
 * and provides functionalities to register, retrieve, update, and remove train departures.
 * It maintains a list of train departures and enables operations for managing and accessing
 * departure-related information such as time, train numbers, destinations, tracks, and delays.
 */
public class DepartureRegister {
  private List<TrainDeparture> departures;

  /**
   * Constructs a DepartureRegister given a list of TrainDepartures.
   *
   * @param departures a list of departures.
   */
  public DepartureRegister(List<TrainDeparture> departures) {
    this.departures = departures;
  }

  /**
   *Constructs a DepartureRegister.
   */
  public DepartureRegister() {
     departures = new ArrayList<>();
  }

  /**
   * Get the departures list.
   *
   * @return departures list.
   */
  public List<TrainDeparture> getDepartures() {
    return departures;
  }

  /**
   * Registers a new train departure.
   *
   * @param departure The `TrainDeparture` to be registered.
   * 
   * @throws IllegalArgumentException if the train number already exists in the register.
   */
  public void registerNewDeparture(TrainDeparture departure) {
    if (doesNumberExist(departure.getTrainNumber())) {
      throw new IllegalArgumentException("The given Train number already exists in the Register");
    }
    departures.add(departure);
  }


  /**
   * Checks if a train number already exists in the departure register.
   *
   * @param trainNumber The train number to check for existence.
   * @return `true` if the train number exists in the register, `false` otherwise.
   */
  public boolean doesNumberExist(int trainNumber) {
    return departures.stream().anyMatch(departure -> departure.getTrainNumber() == trainNumber);
  }


  /**
   * Gets the TrainDeparture with the specified train number.
   *
   * @param trainNumber The train number to look for.
   * @return The TrainDeparture with the given train number, or {@code null} if not found.
   */
  public TrainDeparture getDepartureOnNumber(int trainNumber) {
    return departures.stream()
    .filter(departure -> departure.getTrainNumber() == trainNumber)
    .findFirst()
    .orElse(null);
  }

  /**
   * Retrieves a list of train departures that have a specified destination.
   *
   * @param destination The destination to filter departures by.
   * @return A list of TrainDeparture objects with the specified destination.
   */
  public List<TrainDeparture> getDeparturesOnDestination(String destination) {
    return departures.stream()
      .filter(departure -> departure.getDestination().equals(destination))
      .collect(Collectors.toList());
  }

  /**
   * Removes train departures from the collection if their scheduled departure time plus delay
   * is before the specified time.
   *
   * @param time the time to remove departures before.
   */
  public void removeDeparted(LocalTime time) {
    // Was iterator but changed to use remove if as that is faster according to:
    // https://stackoverflow.com/questions/33182102/difference-in-lambda-performances
    // Aswell as being more consice
    departures.removeIf(departure -> {
      // Handles edgecase where Departuretime + delay is after midnight 
      // Handels it by not removing if departuretime without delay is after or equal to time
      if (!departure.getDepartureTime().isBefore(time)) {
        return false;
      }
      return departure.getDepartureTime().plus(departure.getDelay()).isBefore(time);
    });
  }
    
  /**
   * Returns a sorted list of TrainDeparture objects based on their departure times.
   *
   * @return A new list containing TrainDeparture objects sorted by departure time.
   */
  public List<TrainDeparture> getSortedList() {
    List<TrainDeparture> output = new ArrayList<>(departures);
    output.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    return output;
  }

  /**
   * Returns a list of train numbers for all departures in the register.
   *
   * @return a list of train numbers
   */
  public List<Integer> getTrainNumbers() {
    return departures.stream()
        .map(TrainDeparture::getTrainNumber)
        .collect(Collectors.toList());
  }

  /**
   * Sets the track number for a train with the given train number.
   *
   * @param track The track number to set.
   * @param trainNumber The train number of the train to update.
   * @throws IllegalArgumentException If no train is found with the given train number.
   */
  public void setTrackOnTrainNumber(int track, int trainNumber) {
    departures.stream().filter(departure -> departure.getTrainNumber() == trainNumber)
      .findFirst()
      .orElseThrow(() -> new IllegalArgumentException("No train found with given trainNumber"))
      .setTrack(track);
  }
  
  /**
   * Sets the delay for a train with the specified train number.
   *
   * @param delay The duration of the delay
   * @param trainNumber The train number of the train to set the delay for
   * @throws IllegalArgumentException if no train is found with the given train number
   */
  public void setDelayOnTrainNumber(Duration delay, int trainNumber) {
    departures.stream().filter(departure -> departure.getTrainNumber() == trainNumber)
      .findFirst()
      .orElseThrow(() -> new IllegalArgumentException("No train found with given trainNumber"))
      .setDelay(delay);
  }

  /**
   * Removes a departure from the register based on the train number.
   *
   * @param trainNumber the train number of the departure to be removed
   * @throws IllegalArgumentException if no train is found with the given train number
   */
  public void removeOnTrainNumber(int trainNumber) {
    boolean removed = departures.removeIf(departure -> departure.getTrainNumber() == trainNumber);

    if (!removed) {
      throw new IllegalArgumentException("No train found with the given trainNumber");
    }
  }
}
