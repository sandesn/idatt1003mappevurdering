package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Represents information about a train departure.
 *
 * <p>{@code TrainDeparture} encapsulates details about a specific train's departure,
 * such as the departure time, train number, line, destination, track, and delay.
 * Instances of this class serve as containers for storing train departure information.
 *
 * <p>The departure time is represented using {@code LocalTime},
 * delay is represented using {@code Duration}, 
 * line and destination is represented as a {@code String}
 * and trainNumber and track is represented as an {@code int}
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private Duration delay;

  /**
   * Constructs a TrainDeparture with specified departure information with delay.
   *
   * @param departureTime The time of departure.
   * @param line The train line name.
   * @param trainNumber The train number.
   * @param destination The destination of the train.
   * @param track The track number.
   * @param delay The delay duration. Should be non-negative.
   * 
   * @throws IllegalArgumentException if the delay, track or trainNumber is negative.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber, String destination, 
      int track, Duration delay) {
    if (trainNumber < 0) {
      throw new IllegalArgumentException("trainNumber cannot be negative");
    }
    setTrack(track);
    setDelay(delay);

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
  }
  
  /**
   * Constructs a TrainDeparture with specified departure information without delay.
   *
   * @param departureTime The time of departure.
   * @param line The train line name.
   * @param trainNumber The train number.
   * @param destination The destination of the train.
   * @param track The track number. Must not be negative.
   * 
   * @throws IllegalArgumentException if trainNumber or track is negative.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber,
      String destination, int track) {
    if (trainNumber < 0) {
      throw new IllegalArgumentException("trainNumber cannot be negative");
    }
    setTrack(track);

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.delay = Duration.ZERO;
  }

  /**
  * Get the departure time of the train.
  *
  * @return The departure time.
  */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Get the train line name.
   *
   * @return The train line name.
   */
  public String getLine() {
    return line;
  }

  /**
   * Get the train number.
   *
   * @return The train number.
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Get the destination of the train.
   *
   * @return The destination.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Get the track number.
   *
   * @return The track number.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Get the delay duration of the train.
   *
   * @return The delay duration.
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Set the delay duration for the train. Should be non-negative.
   *
   * @param delay The delay duration to set.
   * @throws IllegalArgumentException if the delay is negative.
   */
  public void setDelay(Duration delay) {
    if (delay.isNegative()) {
      throw new IllegalArgumentException("Delay cannot be negative");
    }
    this.delay = delay;
  }
  
  /**
   * Sets the track number for the train departure.
   *
   * @param track the track number to be set
   * @throws IllegalArgumentException if the track number is negative, except for -1
   */
  public void setTrack(int track) {
    if (track < -1) {
      throw new IllegalArgumentException("Track cannot be negative, except for -1");
    }
    this.track = track;
  }

  /**
   * Returns a string representation of the TrainDeparture object.
   *
   * @return A formatted string with train departure information.
   */
  @Override
  public String toString() {
    String output = "---------- Train departure ----------\n";
    output += "Departure time: " + getDepartureTime() + "\n";
    output += "Train number: " + getTrainNumber() + "\n";
    output += "Train line: " + getLine() + "\n";
    output += "Track: " + getTrack() + "\n";
    output += "Destination: " + getDestination() + "\n";
    output += "Delay: " + String.format("%02d:%02d", delay.toHours(), delay.toMinutesPart()) + "\n";
    return output;
  }

  /**
   * Returns a formatted string representation of the train departure information.
   * The string contains the departure time, train number, line, track, destination, and delay.
   *
   * @return a formatted string representation of the train departure information
   */
  public String stringTableFormat() {
    String output = getDepartureTime() + "\t\t";
    output += getLine() + "\t";
    output += getTrainNumber() + "\t\t";
    if (getDestination().length() < 8) {
      output += getDestination() + "\t\t";
    } else {
      output += getDestination() + "\t";
    }
    if (!delay.isZero()) {
      output += String.format("%02d:%02d", delay.toHours(), delay.toMinutesPart()) + "\t";
    } else {
      output += "\t";
    }
    if (getTrack() != -1) {
      output += getTrack() + "\t";
    } 
    return output;
  }
}
