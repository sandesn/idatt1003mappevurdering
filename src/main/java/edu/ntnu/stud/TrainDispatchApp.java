package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

/**
 * This is the main class for the train dispatch application.
 *
 * <p>{@code TrainDispatchApp} provides functionality for handling 
 * train departure operations via a text-based interface.
 * {@code TrainDispatchApp} uses the class {@code UserInterface} for user input and output
 * and stores the inforamtion in a {@code DepartureRegister}
 */
public class TrainDispatchApp {
  private static boolean repeat = true;
  private static final int LIST_ALL_DEPARURES = 1;
  private static final int ADD_NEW_DEPARTURE = 2;
  private static final int REMOVE_DEPARTURE = 3;
  private static final int ASSIGN_TRACK = 4;
  private static final int CHANGE_DELAY = 5;
  private static final int SEARCH_ON_TRAIN_NUMBER = 6;
  private static final int SEARCH_ON_DESTINATION = 7;
  private static final int UPPDATE_CLOCK = 8;
  private static final int EXIT = 9;

  private static Scanner scanner;
  private static UserInterface userInterface;
  private static DepartureRegister register;
  private static LocalTime clock;

  
  /**
   * The main function of the program.
   *
   * @param args command line arguments not used.
   */
  public static void main(String[] args) {
    init();

    while (repeat) {
      int answer = userInterface.inputQuestion(scanner);
      switch (answer) {
        case (LIST_ALL_DEPARURES):
          listAllDepartures();
          break;

        case (ADD_NEW_DEPARTURE):
          addNewDeparture();
          break;

        case (REMOVE_DEPARTURE):
          removeDeparture();
          break;

        case (ASSIGN_TRACK):  
          assignTrack();
          break;
          
        case (CHANGE_DELAY):  
          changeDelay();
          break;

        case (SEARCH_ON_TRAIN_NUMBER):  
          searchOnTrainNumber();
          break;

        case (SEARCH_ON_DESTINATION):  
          searchOnDestination();
          break;
        
        case (UPPDATE_CLOCK):  
          uppdateClock();
          break;
          
        case (EXIT):
          exitApplication();
          break;

        default:
          invalidSelection();
          break;
      }
    }
  }

  private static void init() {
    TrainDispatchApp.scanner = new Scanner(System.in);
    TrainDispatchApp.userInterface = new UserInterface();
    TrainDispatchApp.register = new DepartureRegister();
    TrainDispatchApp.clock = LocalTime.MIN;

    // Only for testing
    fillWithTestData(register);
  }
  
  /**
   * Fills the given DepartureRegister with test data.
   *
   * @param register the DepartureRegister to fill with test data
   */
  private static void fillWithTestData(DepartureRegister register) {
    TrainDeparture departure1 = new TrainDeparture(LocalTime.of(10, 0), "Line A", 101,
        "Station X", -1, Duration.ofMinutes(5));
    TrainDeparture departure2 = new TrainDeparture(LocalTime.of(11, 30), "Line B", 202,
        "Station Y", -1, Duration.ofMinutes(0));
    TrainDeparture departure3 = new TrainDeparture(LocalTime.of(12, 15), "Line C", 303,
        "Station Z", 3);
    TrainDeparture departure4 = new TrainDeparture(LocalTime.of(13, 45), "Line D", 404,
        "Station W", 4, Duration.ofMinutes(10));
    
    register.registerNewDeparture(departure2);
    register.registerNewDeparture(departure1);
    register.registerNewDeparture(departure3);
    register.registerNewDeparture(departure4);
  }

  /**
   * Lists all train departures.
   */
  private static void listAllDepartures() {
    List<TrainDeparture> departures = register.getSortedList();
    userInterface.printTable(departures);
  }

  /**
   * Adds a new train departure to the register.
   */
  private static void addNewDeparture() {
    TrainDeparture departure = userInterface.getTrainDepartureFromUser(scanner,
        register.getTrainNumbers(), clock);
    register.registerNewDeparture(departure);
  }

  /**
   * Assigns a track to a train.
   */
  private static void assignTrack() {
    int trainNumber = userInterface.getTrainNumberFromUser(scanner);
    if (register.doesNumberExist(trainNumber)) {
      int track = userInterface.getTrackFromUser(scanner);
      register.setTrackOnTrainNumber(track, trainNumber);
      System.out.println("Track changed succesfully!");
    } else {
      System.out.println("Could not find a departure with given train number");
    }
  }

  /**
   * Changes the delay of a departure.
   */
  private static void changeDelay() {
    int trainNumber = userInterface.getTrainNumberFromUser(scanner);
    if (register.doesNumberExist(trainNumber)) {
      Duration delay = userInterface.getDelayFromUser(scanner);
      register.setDelayOnTrainNumber(delay, trainNumber);
    } else {
      System.out.println("Could not find a departure with given train number");
    }    
  }

  /**
   * Searches for a train departure based on the train number entered by the user.
   */
  private static void searchOnTrainNumber() {
    int trainNumber = userInterface.getTrainNumberFromUser(scanner);
    TrainDeparture departure = register.getDepartureOnNumber(trainNumber);
    if (departure != null) {
      System.out.println(departure);
    } else {
      System.out.println("Could not find a departure with given train number");
    }
  }

  /**
   * Searches for train departures based on destination entered by the user.
   */
  private static void searchOnDestination() {
    String destination = userInterface.getDestinationFromUser(scanner);
    List<TrainDeparture> departures = register.getDeparturesOnDestination(destination);
    if (!departures.isEmpty()) {
      userInterface.printTable(departures);
    } else {
      System.out.println("Could not find any departues with given destination");
    }
  }

  /**
   * Uppdates the clock based on input from user.
   */ 
  private static void uppdateClock() {
    LocalTime time = userInterface.getNewClockTimeFromUser(scanner, clock);
    clock = time;

    register.removeDeparted(clock);
    System.out.println("Clock successfully uppdated");
  }

  /**
   * Assigns a track to a train.
   */
  private static void removeDeparture() {
    int trainNumber = userInterface.getTrainNumberFromUser(scanner);
    if (register.doesNumberExist(trainNumber)) {
      register.removeOnTrainNumber(trainNumber);
      System.out.println("Train removed succesfully!");
    } else {
      System.out.println("Could not find a departure with given train number");
    }
  }

  /**
  * Handles the necessary closing and exiting of the program.
  */
  private static void exitApplication() {
    System.out.println("Closing application");
    scanner.close();
    System.exit(0);
  }

  /**
   * Handles an invalid selection from the user.
   */
  private static void invalidSelection() {
    System.out.println("Unrecognized menu selected..");
  }
}
