package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

/**
 * This class manages text-based input and output for the train dispatch application.
 * It facilitates user interaction through a console interface for managing train departures.
 *
 * <p>{@code UserInterface} serves as the interface between the user 
 * and the application's core functionalities.
 * It provides methods for prompting users, receiving inputs, and displaying information
 * related to train departures stored in the {@code DepartureRegister}.
 */
public class UserInterface {
  /**
   * Prints the departure table.
   *
   * @param departures list of departures to be printed
   */
  public void printTable(List<TrainDeparture> departures) {
    String output = "Departure Time\tline\tTrainNumber\tDestination\tDelay\tTrack\n";
    for (TrainDeparture departure : departures) {
      output += departure.stringTableFormat() + "\n";
    }

    System.out.println(output);

  }

  /**
   * Prompts the user with a menu of options and reads the user's choice.
   *
   * @param scanner the Scanner object used to read user input
   * @return the user's choice as an integer
   */
  public int inputQuestion(Scanner scanner) {
    while (true) {
      System.out.println("\n***** Train Dispatch Application *****\n");
      System.out.println("1. List all train departures");
      System.out.println("2. Add new departure");
      System.out.println("3. Remove departure on train number");
      System.out.println("4. Assign new track");
      System.out.println("5. Set new delay");
      System.out.println("6. Search for departure on train number");
      System.out.println("7. Search for departure on destination");
      System.out.println("8. Uppdate clock");
      System.out.println("9. Quit");
      System.out.println("\n Please enter a number between 1 and 9.\n");

      if (scanner.hasNextInt()) {
        int choice = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        return choice;
      } else {
        System.out.println("Please enter an integer");
        scanner.nextLine(); // Consume non int input
      }
    }
  }

  /**
   * Gets the departure time from the user.
   *
   * @param scanner the scanner object used for user input
   * @return the departure time entered by the user
   */
  public LocalTime getDepartureTimeFromUser(Scanner scanner, LocalTime clock) {
    while (true) {
      System.out.println("Enter the departure time (hhmm)");
      String userInput = scanner.nextLine();
      if (userInput.length() == 4) {
        int hour = Integer.parseInt(userInput.substring(0, 2));
        int minutes = Integer.parseInt(userInput.substring(2));

        if (hour >= 0 && hour < 24 && minutes >= 0 && minutes <= 60) {
          if (LocalTime.of(hour, minutes).isBefore(clock)) {
            System.out.println("Invalid time, time must be after current clock: " + clock);
          } else {
            return LocalTime.of(hour, minutes);
          }
        } else {
          System.out.println("Invalid time, time must be between 0000 and 2359");
        }
      } else {
        System.out.println("Invalid input, Please enter in the format: hhmm");
      }
    }
  }

  /**
   * Gets the trainNumber from the user.
   *
   * @param scanner the Scanner object used for user input
   * @return the train number entered by the user
   */
  public int getTrainNumberFromUser(Scanner scanner) {
    while (true) {
      System.out.println("Enter the train number");
      if (scanner.hasNextInt()) {
        int userInput = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        if (userInput >= 0) {
          return userInput;
        } else {
          System.out.println("Invalid input, please enter 0 or bigger");
        }
      } else {
        System.out.println("Invalid input, please enter an integer");
        scanner.nextLine(); // Consume invalid input
      }
    }
  }

  /**
   * Gets the Line from the user.
   *
   * @param scanner the Scanner object used for user input
   * @return the Line entered by the user
   */
  public String getLineFromUser(Scanner scanner) {
    System.out.println("Please enter the Train Line");
    while (true) {
      if (scanner.hasNext()) {
        return scanner.nextLine();
      } else {
        System.out.println("Invalid Input, Please enter the Train Line");
      }
    }
  }

  /**
   * Gets the Track from the user.
   *
   * @param scanner the Scanner object used for user input
   * @return the Track entered by the user
   */
  public int getTrackFromUser(Scanner scanner) {
    while (true) {
      System.out.println("Enter the Track, enter -1 if unassigned");
      if (scanner.hasNextInt()) {
        int userInput = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        if (userInput >= -1) {
          return userInput;
        } else {
          System.out.println("Invalid input, please enter -1 or bigger");
          scanner.nextLine(); // Consume invalid input
        }
      } else {
        System.out.println("Invalid input, please enter an integer");
        scanner.nextLine(); // Consume invalid input
      }
    }
  }

  /**
   * Gets the Destination from the user.
   *
   * @param scanner the Scanner object used for user input
   * @return the Destination entered by the user
   */
  public String getDestinationFromUser(Scanner scanner) {
    System.out.println("Please enter the Destination");
    while (true) {
      if (scanner.hasNext()) {
        return scanner.nextLine();
      } else {
        System.out.println("Invalid Input, Please enter the Destination");
      }
    }
  }

  /**
   * Gets the Delay from the user.
   *
   * @param scanner the Scanner object used for user input
   * @return the Delay entered by the user
   */
  public Duration getDelayFromUser(Scanner scanner) {
    while (true) {
      System.out.println("Please enter how much Delay in minutes, enter 0 if no delay.");
      if (scanner.hasNextInt()) {
        int delay = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        if (delay >= 0) {
          return Duration.ofMinutes(delay);
        } else {
          System.out.println("Invalid Input, Delay must be positive");
        }
      } else {
        System.out.println("Invalid Input, please enter a positive integer");
        scanner.nextLine(); // Consume invalid input
      }
    }
  }

  /**
   * Gets the TrainDeparture from the user.
   *
   * @param scanner the Scanner object used for user input
   * @param existingTrainNumbers the list of existing train numbers
   * @return the TrainDeparture entered by the user
   */
  public TrainDeparture getTrainDepartureFromUser(
      Scanner scanner, List<Integer> existingTrainNumbers, LocalTime clock) {
    LocalTime departureTime = getDepartureTimeFromUser(scanner, clock);
    int trainNumber;
    while (true) {
      trainNumber = getTrainNumberFromUser(scanner);
      if (!existingTrainNumbers.contains(trainNumber)) {
        break;
      } else {
        System.out.println("Train number already exists. Please enter a unique train number.");
      }
    }
    String line = getLineFromUser(scanner);
    int track = getTrackFromUser(scanner);
    String destination = getDestinationFromUser(scanner);
    Duration delay = getDelayFromUser(scanner);
    return new TrainDeparture(departureTime,
        line, trainNumber, destination, track, delay);
  }

  /**
   * Retrieves a new clock time from the user.
   *
   * @param scanner the scanner object used to read user input
   * @return the new clock time as a LocalTime object
   */
  public LocalTime getNewClockTimeFromUser(Scanner scanner, LocalTime oldClock) {
    while (true) {
      System.out.println("Enter the new clock time (hhmm)");
      String userInput = scanner.nextLine();
      if (userInput.length() == 4) {
        int hour = Integer.parseInt(userInput.substring(0, 2));
        int minutes = Integer.parseInt(userInput.substring(2));

        if (hour >= 0 || hour < 24 || minutes >= 0 || minutes <= 60) {
          if (LocalTime.of(hour, minutes).isBefore(oldClock)) {
            System.out.println("Invalid time, time must be after old clock: " + oldClock);
          } else {
            return LocalTime.of(hour, minutes);
          }
        } else {
          System.out.println("Invalid time, time must be between 0000 and 2359");
        }
      } else {
        System.out.println("Invalid input, Please enter in the format: hhmm");
      }
    }
  }
}
