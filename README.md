# Portfolio project IDATT1003 - 2023

STUDENT NAME = "Sander Sandvik Nessa"  
STUDENT ID = "106386"

## Project description

This is a system for a simple Train dispatch system. The system is limited to a text based UI and only handles train departures from a single station. It contains the classes: TrainDeparture, DepartureRegister, UserInterface and TrainDispatchApp.

## Project structure

The project is managed using maven to handle dependencies. The source files for the program is stored under src/main, while the test classes is stored under src/test.

## Link to repository

https://gitlab.stud.idi.ntnu.no/sandesn/idatt1003mappevurdering/

## How to run the project

The main method is in the TrainDispatchApp class. The program will use the terminal as input and output. And the UI will be text based.

## How to run the tests

Run tests using maven. The command is : "mvn test". Also possible to run test using vscode GUI/sidebar.